package com.assaignment.grid;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Grid {

  private Set<Cell> liveCells;

  Grid(Set<Cell> input) {

    this.liveCells = input;
  }

  public Grid nextGeneration(){

    return new Grid(nextTick());
  }

  private Set<Cell> createGrid() {
    Iterator it = this.liveCells.iterator();
    Set<Cell> possibleNeighbours=new HashSet<>();
    while (it.hasNext()) {
      Cell cell = (Cell) it.next();
      Set<Cell> possibleCellNeighbours = cell.getPossibleNeighbours();
      possibleCellNeighbours.contains(cell);
      possibleNeighbours.addAll(possibleCellNeighbours);
    }
    return possibleNeighbours;
  }

  private Set<Cell> nextTick() {
    Set<Cell> nextGeneration=new HashSet<>();
    Set<Cell> possibleNeighbours=createGrid();
    Iterator it = possibleNeighbours.iterator();
    while (it.hasNext()) {
      Cell cell = (Cell) it.next();
      Cell cellInNextTick=cell.inNextTick(liveCells);
      if (cellInNextTick.isLive()) {
        nextGeneration.add(cell);
      }
    }
    return nextGeneration;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Grid that = (Grid) o;

    return liveCells != null ? liveCells.equals(that.liveCells) : that.liveCells == null;

  }

  @Override
  public int hashCode() {
    return liveCells != null ? liveCells.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "Output : "+liveCells;
  }
}
