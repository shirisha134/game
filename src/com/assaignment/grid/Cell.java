package com.assaignment.grid;

import java.util.HashSet;
import java.util.Set;

public class Cell {
  private int x;
  private int y;
  private boolean status;

  Cell(int x, int y) {
    this(x,y,false);
  }
  Cell(int x, int y,boolean status) {
    this.x = x;
    this.y = y;
    this.status= status;
  }

  private static HashSet<Cell> directions() {
    HashSet<Cell> neighbours = new HashSet<>();
    neighbours.add(new Cell(-1, -1));
    neighbours.add(new Cell(0, -1));
    neighbours.add(new Cell(1, -1));
    neighbours.add(new Cell(-1, 0));
    neighbours.add(new Cell(1, 0));
    neighbours.add(new Cell(-1, 1));
    neighbours.add(new Cell(0, 1));
    neighbours.add(new Cell(1, 1));
    return neighbours;
  }

  private int liveNeighboursCount(Set<Cell> liveCells) {

    int count = 0;
    HashSet<Cell> directions = directions();
    for (Cell p : directions) {
      Cell newPoint = new Cell(this.x + p.x, this.y + p.y);
      if (liveCells.contains(newPoint)) {
        count++;
      }
    }
    return count;
  }

  public Set<Cell> getPossibleNeighbours() {

    Set<Cell> directions = directions();
    Set<Cell> possibleNeighbours = new HashSet<>();
    for (Cell p : directions) {
      Cell newPoint = new Cell(this.x + p.x, this.y + p.y);
      possibleNeighbours.add(newPoint);
    }
    return possibleNeighbours;
  }

  public Cell inNextTick(Set<Cell> liveCells) {
    int liveCount = liveNeighboursCount(liveCells);

    if (liveCount == 2 && liveCells.contains(this)) {
      return new Cell(this.x,this.y,true);
    }
    if (liveCount == 3) {
      return new Cell(this.x,this.y,true);
    }
    return new Cell(this.x,this.y);
  }


  @Override
  public String toString() {
    return "(" +
        "" + x +
        ", " + y + ")";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Cell point = (Cell) o;

    if (x != point.x) return false;
    return y == point.y;

  }

  @Override
  public int hashCode() {
    int result = x;
    result = 31 * result + y;
    return result;
  }

  public boolean isLive() {
    return status;
  }
}
