package com.assaignment.grid;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;

public class GameOfLife {
  public static void main(String args[]) throws IOException {
    HashSet<Cell> input = new HashSet<>();
    FileReader inputFile = new FileReader("src/com/assaignment/grid/input.txt");

    Scanner s = new Scanner(inputFile);

    while (s.hasNextLine()) {
      String[] eachLine = s.nextLine().split(", ");
      Cell newCell = new Cell(Integer.parseInt(eachLine[0]), Integer.parseInt(eachLine[1]));
      input.add(newCell);
    }
    Grid given = new Grid(input);
    Grid outputGrid = given.nextGeneration();
    File outputFile =new File("src/com/assaignment/grid/output.txt");
    FileWriter fileWriter = new FileWriter(outputFile);
    BufferedWriter outputWriter = new BufferedWriter(fileWriter);
    outputWriter.write(outputGrid.toString());
    outputWriter.close();

  }
}


