package com.assaignment.grid;

import org.junit.Test;
import org.junit.Assert;

import java.util.HashSet;

public class GridTest {

  @Test
  public void shouldReturnEmptyHashSet() {
    HashSet<Cell> points = new HashSet<>();
    points.add(new Cell(1, 0));
    Grid given = new Grid(points);
    Grid actual = given.nextGeneration();
    Grid expected = new Grid(new HashSet<>());
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void shouldReturnStillLife() {
    HashSet<Cell> points = new HashSet<>();
    points.add(new Cell(1, 1));
    points.add(new Cell(1, 2));
    points.add(new Cell(2, 1));
    points.add(new Cell(2, 2));
    Grid given = new Grid(points);
    Grid actual = given.nextGeneration();

    HashSet<Cell> expectedPoints = new HashSet<>();
    expectedPoints.add(new Cell(1, 1));
    expectedPoints.add(new Cell(1, 2));
    expectedPoints.add(new Cell(2, 1));
    expectedPoints.add(new Cell(2, 2));
    Grid expected = new Grid(expectedPoints);

    Assert.assertEquals(expected, actual);
  }

  @Test
  public void shouldReturnStillLifeAgain() {
    HashSet<Cell> points = new HashSet<>();
    points.add(new Cell(0, 1));
    points.add(new Cell(1, 0));
    points.add(new Cell(2, 1));
    points.add(new Cell(0, 2));
    points.add(new Cell(1, 2));
    Grid given = new Grid(points);
    Grid actual = given.nextGeneration();

    HashSet<Cell> expectedPoints = new HashSet<>();
    expectedPoints.add(new Cell(0, 1));
    expectedPoints.add(new Cell(1, 0));
    expectedPoints.add(new Cell(2, 1));
    expectedPoints.add(new Cell(0, 2));
    expectedPoints.add(new Cell(1, 2));
    Grid expected = new Grid(expectedPoints);


    Assert.assertEquals(expected, actual);
  }

  @Test
  public void shouldReturnOscillator() {
    HashSet<Cell> points = new HashSet<>();
    points.add(new Cell(1, 1));
    points.add(new Cell(1, 0));
    points.add(new Cell(1, 2));
    Grid given = new Grid(points);
    Grid actual = given.nextGeneration();

    HashSet<Cell> expectedPoints = new HashSet<>();
    expectedPoints.add(new Cell(0, 1));
    expectedPoints.add(new Cell(1, 1));
    expectedPoints.add(new Cell(2, 1));
    Grid expected = new Grid(expectedPoints);
    Assert.assertEquals(actual, expected);
  }

  @Test
  public void shouldReturnTwoPhaseOscillator() {
    HashSet<Cell> points = new HashSet<>();
    points.add(new Cell(1, 1));
    points.add(new Cell(1, 2));
    points.add(new Cell(1, 3));
    points.add(new Cell(2, 2));
    points.add(new Cell(2, 3));
    points.add(new Cell(2, 4));
    Grid given = new Grid(points);
    Grid actual = given.nextGeneration();

    HashSet<Cell> expectedPoints = new HashSet<>();
    expectedPoints.add(new Cell(0, 2));
    expectedPoints.add(new Cell(1, 1));
    expectedPoints.add(new Cell(1, 4));
    expectedPoints.add(new Cell(2, 1));
    expectedPoints.add(new Cell(2, 4));
    expectedPoints.add(new Cell(3, 3));
    Grid expected = new Grid(expectedPoints);

    Assert.assertEquals(expected, actual);
  }

  @Test
  public void shouldGrowGridInNegativeDirection() {
    HashSet<Cell> points = new HashSet<>();
    points.add(new Cell(0, 0));
    points.add(new Cell(0, 1));
    points.add(new Cell(0, 2));
    points.add(new Cell(1, 0));
    points.add(new Cell(1, 1));
    points.add(new Cell(1, 2));
    points.add(new Cell(2, 0));
    points.add(new Cell(2, 1));
    points.add(new Cell(2, 2));
    Grid given = new Grid(points);
    Grid actual = given.nextGeneration();

    HashSet<Cell> expectedPoints = new HashSet<>();
    expectedPoints.add(new Cell(1, -1));
    expectedPoints.add(new Cell(0, 0));
    expectedPoints.add(new Cell(0, 2));
    expectedPoints.add(new Cell(-1, 1));
    expectedPoints.add(new Cell(3, 1));
    expectedPoints.add(new Cell(2, 0));
    expectedPoints.add(new Cell(2, 2));
    expectedPoints.add(new Cell(1, 3));
    Grid expected = new Grid(expectedPoints);

    Assert.assertEquals(expected, actual);
  }

}