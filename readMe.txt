_____________________________________________________
                    GameOfLife
_____________________________________________________

This is the implementation of GameOfLife with the help of HashSet Collection.

Give input in the input text file,in the fallowing format,

                        X-coordinate, Y-Coordinate

A coordinate pair per line.

Output is in the outputFile, which represents the next tick of the given grid(which contains only live cells).
